#!/bin/bash 

# Create Variables for ResourceGroup and Region
RgName="MyResourceGroup"
Location="eastus"

# Create a Resource Group 
az group create \
--name $RgName \
--location $Location

# Create a Virtual Network with a Front-End Subnet
az network vnet create \
--name devops-Vnet \
--resource-group $RgName \
--location $Location \
--address-prefix 10.0.0.0/16 \
--subnet-name MySubnet-FrontEnd \
--subnet-prefix 10.0.1.0/24 

#az network vnet subnet create --address-prefixes 10.0.2.0/24  --name MySubnet-BackEnd --resource-group $RgName --location $Location --vnet-name devops-Vnet

# Create a Network Security Group for the Front-End Subnet
az network nsg create \
--resource-group $RgName \
--name MyNsg-FrontEnd \
--location $Location

# Create a NSG rule to allow HTTP Traffic in From the Internet to the Front-End Subnet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-FrontEnd \
--name Allow-HTTP-All \
--access Allow \
--protocol Tcp \
--direction Inbound \
--priority 100 \
--source-address-prefix Internet \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 80 

# Create a NSG rule to allow SSH Traffic in From the Internet to the Front-End Subnet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-FrontEnd \
--name Allow-SSH-All \
--access Allow \
--protocol Tcp \
--direction Inbound \
--priority 300 \
--source-address-prefix Internet \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 22

# Create a NSG rule to allow RDP Traffic in From the Internet to the Front-End Subnet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-FrontEnd \
--name Allow-RDP-All \
--access Allow \
--protocol Tcp \
--direction Inbound \
--priority 400 \
--source-address-prefix Internet \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 3389

# Associate the Front-End NSG to The Front-End Subnet 
az network vnet subnet update \
--vnet-name devops-Vnet \
--name MySubnet-FrontEnd \
--resource-group $RgName \
--network-security-group MyNsg-FrontEnd


# Create a Linux Server Virtual Machine in the Front-End Subnet
az vm create \
--resource-group $RgName \
--name MyVm-Linux \
--image UbuntuLTS \
--admin-username azureadmin \
--admin-password Azure@123456 \
--size Standard_D2s_v3 \
--data-disk-sizes-gb 128 128 \
--storage-sku Standard_LRS

# Create a Windows Server Virtual Machine in the Front-End Subnet
az vm create \
--resource-group $RgName \
--name MyVm-Win \
--image Win2016Datacenter \
--admin-username azureadmin \
--admin-password Azure@123456 \
--size Standard_D2s_v3 \
--data-disk-sizes-gb 128 128 \
--storage-sku Standard_LRS
New-AzResourceGroup -Name demopowershell -Location EastUS

$virtualNetwork = New-AzVirtualNetwork `
  -ResourceGroupName demopowershell `
  -Location EastUS `
  -Name myvnet `
  -AddressPrefix 10.0.0.0/16

$subnetConfig = Add-AzVirtualNetworkSubnetConfig `
  -Name default `
  -AddressPrefix 10.0.0.0/24 `
  -VirtualNetwork $virtualNetwork

$virtualNetwork | Set-AzVirtualNetwork

Todays Agenda :

    -   Provision Windows & Linux VM 
    -   Download, Install & Configure WebServer part of Windows & Linux 
    -   Deploy a Simple HTMl Website on Windows and Linux 
    -   Validate 
    -   Clean-up 

- Creating Budget Alert - Manage Action Groups 

- Roles  

- Creating Vnet 

- Creating Subnet 

- Creating Network Security Groups 

- Creating VM inside Subnet 

Completed Topics :

Moduel-2 : Manage users and groups in Azure Active Directory

    -   Introduction
    -   What is Azure Active Directory?
    -   Create and manage users
    -   Create and manage groups
    -   Use roles to control resource access
    -   Connect Active Directory to Azure AD with Azure AD Connect
    -   Summary
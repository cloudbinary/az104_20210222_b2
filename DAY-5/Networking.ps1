<#
Networking :

1. Vnet

2. Subnets

3. Network Security Groups 

4. Azure Bastion

5. Azure Application Gateway

6. Azure DNS 

7. Azure ExpressRoute

8. Traffic Manager 

9. Load Balancer

10. Content Delivery Network 

11. Network Watcher

12. VPN Gateway

13. Virtual WAN

14. Web Application Firewall

15. Azure Firewall

16. Azure Firewall Manager 

17. Azure Front Door

18. Azure Internet Analyzer 

19. Azure Privite Link

20. Azure DDoS Protection

#>
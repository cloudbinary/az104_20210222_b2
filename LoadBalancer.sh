#!/bin/bash 

# STEP-1 : RG 

az group create \
    --name CreatePubLBQS-rg \
    --location eastus

# STEP-2 : Vnet - Create a virtual network

az network vnet create \
    --resource-group CreatePubLBQS-rg \
    --location eastus \
    --name myVNet \
    --address-prefixes 10.1.0.0/16 \
    --subnet-name myBackendSubnet \
    --subnet-prefixes 10.1.0.0/24

# STEP-3 : Create a network security group

az network nsg create \
    --resource-group CreatePubLBQS-rg \
    --name myNSG

# STEP-4 : Create a network security group rule

az network nsg rule create \
    --resource-group CreatePubLBQS-rg \
    --nsg-name myNSG \
    --name myNSGRuleHTTP \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 80 \
    --access allow \
    --priority 200

# STEP-5 : Create backend servers - Basic

# Create network interfaces for the virtual machines

array=(myNicVM1 myNicVM2 myNicVM3)
  for vmnic in "${array[@]}"
  do
    az network nic create \
        --resource-group CreatePubLBQS-rg \
        --name $vmnic \
        --vnet-name myVNet \
        --subnet myBackEndSubnet \
        --network-security-group myNSG
  done

# STEP-6 : Create availability set for virtual machines

az vm availability-set create \
    --name myAvSet \
    --resource-group CreatePubLBQS-rg \
    --location eastus

# STEP-7 : Create virtual machines

az vm create \
    --resource-group CreatePubLBQS-rg \
    --name myVM1 \
    --nics myNicVM1 \
    --image win2016datacenter \
    --admin-username azureuser \
    --availability-set myAvSet \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 
    --no-wait

az vm create \
    --resource-group CreatePubLBQS-rg \
    --name myVM2 \
    --nics myNicVM2 \
    --image win2016datacenter \
    --admin-username azureuser \
    --availability-set myAvSet \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 
    --no-wait

az vm create \
    --resource-group CreatePubLBQS-rg \
    --name myVM3 \
    --nics myNicVM3 \
    --image win2016datacenter \
    --admin-username azureuser \
    --availability-set myAvSet \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 
    --no-wait

# STEP-8 : Create a public IP address - Basic

az network public-ip create \
    --resource-group CreatePubLBQS-rg \
    --name myPublicIP \
    --sku Basic

# STEP-9 : Create basic load balancer

az network lb create \
    --resource-group CreatePubLBQS-rg \
    --name myLoadBalancer \
    --sku Basic \
    --public-ip-address myPublicIP \
    --frontend-ip-name myFrontEnd \
    --backend-pool-name myBackEndPool

# STEP-10 : Create the health probe

az network lb probe create \
    --resource-group CreatePubLBQS-rg \
    --lb-name myLoadBalancer \
    --name myHealthProbe \
    --protocol tcp \
    --port 80

# STEP-11 : Create the load balancer rule

az network lb rule create \
    --resource-group CreatePubLBQS-rg \
    --lb-name myLoadBalancer \
    --name myHTTPRule \
    --protocol tcp \
    --frontend-port 80 \
    --backend-port 80 \
    --frontend-ip-name myFrontEnd \
    --backend-pool-name myBackEndPool \
    --probe-name myHealthProbe \
    --idle-timeout 15

# STEP-12  : Add virtual machines to load balancer backend pool

array=(myNicVM1 myNicVM2 myNicVM3)
  for vmnic in "${array[@]}"
  do
    az network nic ip-config address-pool add \
     --address-pool myBackendPool \
     --ip-config-name ipconfig1 \
     --nic-name $vmnic \
     --resource-group CreatePubLBQS-rg \
     --lb-name myLoadBalancer
  done

# STEP-13 : Install IIS

array=(myVM1 myVM2 myVM3)
    for vm in "${array[@]}"
    do
     az vm extension set \
       --publisher Microsoft.Compute \
       --version 1.8 \
       --name CustomScriptExtension \
       --vm-name $vm \
       --resource-group CreatePubLBQS-rg \
       --settings '{"commandToExecute":"powershell Add-WindowsFeature Web-Server; powershell Add-Content -Path \"C:\\inetpub\\wwwroot\\Default.htm\" -Value $($env:computername)"}'
  done

# STEP-14 : Test the load balancer

az network public-ip show \
    --resource-group CreatePubLBQS-rg \
    --name myPublicIP \
    --query ipAddress \
    --output tsv

# Clean up resources
# az group delete --name CreatePubLBQS-rg

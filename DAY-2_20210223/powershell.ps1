<#
PS /home/c> $location="westus"

PS /home/c> echo $location
westus

PS /home/c> $myResourceGroup="cloud-shell-storage-centralindia"

PS /home/c> echo $myResourceGroup
cloud-shell-storage-centralindia

PS /home/c> $myVM="winDemoVM"

PS /home/c> echo $myVM
winDemoVM


#>

$location="westus"
$myResourceGroup="cloud-shell-storage-centralindia"
$imageName="UbuntuLTS"
$myVM="winDemoVM"

New-AzVm -ResourceGroupName $myResourceGroup -Name $myVM -ImageName "myImage" -Location $location
New-AzVm -ResourceGroupName $myResourceGroup -Name $myVM -ImageName $imageName -Location $location
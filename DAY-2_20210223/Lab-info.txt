This lab is provided for hands-on practice.

Please read the information below carefully.
1. Click on "start lab" to set up the lab and you'll see "Launch Lab" as soon as the lab has been setup. This may take 2-5 mins.
2. Login to the Azure portal after clicking "Launch Lab". This will open the Azure portal in a new tab. Do not open the azure portal independently and login. This will create Subscription issues
3. Labs are set up under fair usage policy, please keep a check on resource usage. 

- Region: 

West US, 
West US2, 
East US, 
East US2, 
Central US, and 
Global


- Virtual Machine Instance Types: 
Standard_B1ms, 
Standard_B1s, 
Standard_B2s, 
Standard_D2s_v3, and 
Standard_B4ms


- Virtual Machine OS Flavors: 
Ubuntu 16.04-LTS, 17.10, 14.04.5-LTS, and 18.04-LTS
Windows 2016-Datacenter, 2016-Datacenter-Server-Core, 2016-Datacenter-with-Containers, and 2016-Datacenter-Server-Core-smalldisk

 

-Microsoft.Network/loadBalancers:  Microsoft.Network/loadBalancers/sku.name:Basic

 

-Microsoft.Network/applicationGateways: 

Microsoft.Network/applicationGateways/sku.name: Standard_Small
Microsoft.Network/applicationGateways/sku.tier: Standard
Microsoft.Network/applicationGateways/sku.capacity: 2


-Session Time:
- The Azure Portal access duration per session is 300 mins (5 Hrs)
- The Lab will automatically end after the access duration is complete.
- You can start a fresh session again if required.


- Microsoft.Sql/servers/databases:

Microsoft.Sql/servers/databases/requestedServiceObjectiveName: S1
Microsoft.Sql/servers/databases/sku.tier: Basic | Standard


- AllowOnlyStandardContainerRegistry: Microsoft.ContainerRegistry/registries/Sku.name: Basic


- Microsoft Job collection: Standard


- AllowOnlyBasicPostgresql: B_Gen5_2


- AllowOnlyBasicMySql:B_Gen5_2


- Service Disabled: Azure Marketplace, HD Insight, Azure Active Directory, IoT, Blockchain


Azure Resource Usage:
- Resources created during the Lab session is for learning purposes only.
- Please do NOT plan to use the resources for any other personal/commercial use.
- All resources created during a session will be terminated automatically once the session ends.
- Resources created in previous sessions will NOT be available for reuse.
NOTE: Lab has a fixed monthly subscription attached. Therefore use the resources wisely.

 

You can download the Lab Guides from here.
<#

STEP-1 : 

cloudbinary@Clouds-MacBook-Pro ~ % pwd
/Users/ck

cloudbinary@Clouds-MacBook-Pro ~ % az group create --name webserverRG --location eastus
{
  "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG",
  "location": "eastus",
  "managedBy": null,
  "name": "webserverRG",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": "Microsoft.Resources/resourceGroups"
}


STEP-2 : 

cloudbinary@Clouds-MacBook-Pro ~ % az vm create --resource-group webserverRG --name webserver --image UbuntuLTS --admin-username azureuser --admin-password Azure@123456
Command group 'vm' is experimental and under development. Reference and support levels: https://aka.ms/CLI_refstatus
{- Finished ..
  "fqdns": "",
  "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Compute/virtualMachines/webserver",
  "location": "eastus",
  "macAddress": "00-22-48-20-A8-DD",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.4",
  "publicIpAddress": "52.188.69.230",
  "resourceGroup": "webserverRG",
  "zones": ""
}


STEP-3 :

cloudbinary@Clouds-MacBook-Pro ~ % az vm open-port --port 80 --resource-group webserverRG --name webserver 
Command group 'vm' is experimental and under development. Reference and support levels: https://aka.ms/CLI_refstatus
{- Finished ..
  "defaultSecurityRules": [
    {
      "access": "Allow",
      "description": "Allow inbound traffic from all VMs in VNET",
      "destinationAddressPrefix": "VirtualNetwork",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "*",
      "destinationPortRanges": [],
      "direction": "Inbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/defaultSecurityRules/AllowVnetInBound",
      "name": "AllowVnetInBound",
      "priority": 65000,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "VirtualNetwork",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/defaultSecurityRules"
    },
    {
      "access": "Allow",
      "description": "Allow inbound traffic from azure load balancer",
      "destinationAddressPrefix": "*",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "*",
      "destinationPortRanges": [],
      "direction": "Inbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/defaultSecurityRules/AllowAzureLoadBalancerInBound",
      "name": "AllowAzureLoadBalancerInBound",
      "priority": 65001,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "AzureLoadBalancer",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/defaultSecurityRules"
    },
    {
      "access": "Deny",
      "description": "Deny all inbound traffic",
      "destinationAddressPrefix": "*",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "*",
      "destinationPortRanges": [],
      "direction": "Inbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/defaultSecurityRules/DenyAllInBound",
      "name": "DenyAllInBound",
      "priority": 65500,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "*",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/defaultSecurityRules"
    },
    {
      "access": "Allow",
      "description": "Allow outbound traffic from all VMs to all VMs in VNET",
      "destinationAddressPrefix": "VirtualNetwork",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "*",
      "destinationPortRanges": [],
      "direction": "Outbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/defaultSecurityRules/AllowVnetOutBound",
      "name": "AllowVnetOutBound",
      "priority": 65000,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "VirtualNetwork",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/defaultSecurityRules"
    },
    {
      "access": "Allow",
      "description": "Allow outbound traffic from all VMs to Internet",
      "destinationAddressPrefix": "Internet",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "*",
      "destinationPortRanges": [],
      "direction": "Outbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/defaultSecurityRules/AllowInternetOutBound",
      "name": "AllowInternetOutBound",
      "priority": 65001,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "*",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/defaultSecurityRules"
    },
    {
      "access": "Deny",
      "description": "Deny all outbound traffic",
      "destinationAddressPrefix": "*",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "*",
      "destinationPortRanges": [],
      "direction": "Outbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/defaultSecurityRules/DenyAllOutBound",
      "name": "DenyAllOutBound",
      "priority": 65500,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "*",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/defaultSecurityRules"
    }
  ],
  "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
  "flowLogs": null,
  "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG",
  "location": "eastus",
  "name": "webserverNSG",
  "networkInterfaces": [
    {
      "dnsSettings": null,
      "dscpConfiguration": null,
      "enableAcceleratedNetworking": null,
      "enableIpForwarding": null,
      "etag": null,
      "extendedLocation": null,
      "hostedWorkloads": null,
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkInterfaces/webserverVMNic",
      "ipConfigurations": null,
      "location": null,
      "macAddress": null,
      "name": null,
      "networkSecurityGroup": null,
      "primary": null,
      "privateEndpoint": null,
      "provisioningState": null,
      "resourceGroup": "webserverRG",
      "resourceGuid": null,
      "tags": null,
      "tapConfigurations": null,
      "type": null,
      "virtualMachine": null
    }
  ],
  "provisioningState": "Succeeded",
  "resourceGroup": "webserverRG",
  "resourceGuid": "8a77d2cc-4042-4ab1-9fc3-faebe15128d2",
  "securityRules": [
    {
      "access": "Allow",
      "description": null,
      "destinationAddressPrefix": "*",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "22",
      "destinationPortRanges": [],
      "direction": "Inbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/securityRules/default-allow-ssh",
      "name": "default-allow-ssh",
      "priority": 1000,
      "protocol": "Tcp",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "*",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/securityRules"
    },
    {
      "access": "Allow",
      "description": null,
      "destinationAddressPrefix": "*",
      "destinationAddressPrefixes": [],
      "destinationApplicationSecurityGroups": null,
      "destinationPortRange": "80",
      "destinationPortRanges": [],
      "direction": "Inbound",
      "etag": "W/\"d9ad8354-56a7-4cae-9219-ea336975a9e0\"",
      "id": "/subscriptions/a5bbf16d-a041-4e7e-ba01-6ef4e222fd99/resourceGroups/webserverRG/providers/Microsoft.Network/networkSecurityGroups/webserverNSG/securityRules/open-port-80",
      "name": "open-port-80",
      "priority": 900,
      "protocol": "*",
      "provisioningState": "Succeeded",
      "resourceGroup": "webserverRG",
      "sourceAddressPrefix": "*",
      "sourceAddressPrefixes": [],
      "sourceApplicationSecurityGroups": null,
      "sourcePortRange": "*",
      "sourcePortRanges": [],
      "type": "Microsoft.Network/networkSecurityGroups/securityRules"
    }
  ],
  "subnets": null,
  "tags": {},
  "type": "Microsoft.Network/networkSecurityGroups"
}



#>
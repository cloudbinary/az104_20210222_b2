#!/bin/bash 

# STEP-1 : RG 

az group create --name devopsRG --location eastus

# STEP-2 : Launch 4 Virtual Machines 

az vm create \
    --resource-group devopsRG \
    --name jenkins \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS

az vm create \
    --resource-group devopsRG \
    --name sonarqube \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS

az vm create \
    --resource-group devopsRG \
    --name jfrog \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS

az vm create \
    --resource-group devopsRG \
    --name tomcat \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS
    
az vm create \
    --resource-group devopsRG \
    --name tomcat \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS

az vm create \
    --resource-group devopsRG \
    --name sonarqube \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS

az vm create \
    --resource-group devopsRG \
    --name nagios \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --size Standard_B2s \
    --data-disk-sizes-gb 128 128 \
    --storage-sku Standard_LRS
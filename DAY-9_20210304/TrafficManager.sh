# Create a Traffic Manager profile

# STEP-1 : Create a resource group or User an Existing One

az group create \
    --name myResourceGroup \
    --location eastus

# STEP-2 : Create a Traffic Manager profile

az network traffic-manager profile create \
	--name demo1 \
	--resource-group myResourceGroup \
	--routing-method Priority \
	--path "/" \
	--protocol HTTP \
	--unique-dns-name codewithkesav \
	--ttl 30 \
	--port 80

# STEP-3 : Create web apps

VM1 : eastus 

VM2 : westus 

# STEP-4 : Create web app service plans

az appservice plan create \
    --name web1_eastus \
    --resource-group myResourceGroup \
    --location eastus \
    --sku S1 

az appservice plan create \
    --name web2_westus \
    --resource-group myResourceGroup \
    --location westus \
    --sku S1 

# STEP-5 : Create a web app in the app service plan

az webapp create \
    --name simplilearndev1 \
    --plan web1_eastus \
    --resource-group myResourceGroup

az webapp create \
    --name simplilearndev2 \
    --plan web2_westus \
    --resource-group myResourceGroup

# STEP-6 : Add Traffic Manager endpoints

az webapp show \
--name simplilearndev1 \
--resource-group myResourceGroup \
--query id

# /subscriptions/4175e383-bc77-4c50-9a70-272b4825a12a/resourceGroups/myResourceGroup/providers/Microsoft.Web/sites/simplilearndev1

az webapp show \
--name simplilearndev2 \
--resource-group myResourceGroup \
--query id

# /subscriptions/4175e383-bc77-4c50-9a70-272b4825a12a/resourceGroups/myResourceGroup/providers/Microsoft.Web/sites/simplilearndev2

# STEP-7 : Add WebServer EndPoint to Traffic Manager 

az network traffic-manager endpoint create \
    --name simplilearndev1 \
    --resource-group myResourceGroup \
    --profile-name demo1 \
    --type azureEndpoints \
    --target-resource-id /subscriptions/4175e383-bc77-4c50-9a70-272b4825a12a/resourceGroups/myResourceGroup/providers/Microsoft.Web/sites/simplilearndev1 \
    --priority 1 \
    --endpoint-status Enabled


az network traffic-manager endpoint create \
    --name simplilearndev2 \
    --resource-group myResourceGroup \
    --profile-name demo1 \
    --type azureEndpoints \
    --target-resource-id /subscriptions/4175e383-bc77-4c50-9a70-272b4825a12a/resourceGroups/myResourceGroup/providers/Microsoft.Web/sites/simplilearndev2 \
    --priority 2 \
    --endpoint-status Enabled

# STEP-8 : Test your Traffic Manager profile

az network traffic-manager profile show \
    --name demo1 \
    --resource-group myResourceGroup \
    --query dnsConfig.fqdn

# STEP-9 : View Traffic Manager in Action

az network traffic-manager endpoint update \
     --name devserver1 \
     --resource-group myResourceGroup \
     --profile-name demo1 \
     --type azureEndpoints \
     --endpoint-status Disabled

Validation :

    - server1 : https://simplilearndev1.azurewebsites.net
    - server2 : https://simplilearndev2.azurewebsites.net

http://codewithkesav.trafficmanager.net

    - server1 : https://simplilearndev1.azurewebsites.net
    - server2 : https://simplilearndev2.azurewebsites.net

# STEP-10 : Clean up resources
# az group delete --resource-group myResourceGroup


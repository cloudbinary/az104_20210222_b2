
# STEP-1 : RG 

VNET-1 :

az group create --name myVNET-1RG --location eastus

VNET-2 :
az group create --name myVNET-2RG --location eastus

# STEP-2 : VNET-1
    - SUBNET-1
        - NSG 
            - SSH 

az network vnet create \
    --resource-group myVNET-1RG \
    --location eastus \
    --name Vnet-1 \
    --address-prefixes 10.0.0.0/16 \
    --subnet-name default

# STEP-3 : VNET-2 
    - SUBNET-1
        - NSG 
            - SSH 

az network vnet create \
    --resource-group myVNET-2RG \
    --location eastus \
    --name Vnet-2 \
    --address-prefixes 172.16.0.0/16 \
    --subnet-name default

# STEP-4 : Create a network security group

az network nsg create \
    --resource-group myVNET-1RG \
    --name myNSG1

az network nsg create \
    --resource-group myVNET-2RG \
    --name myNSG2

# STEP-5 : Create a network security group rule

az network nsg rule create \
    --resource-group myVNET-1RG \
    --nsg-name myNSG1 \
    --name myNSGRuleSSH \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 22 \
    --access allow \
    --priority 200

az network nsg rule create \
    --resource-group myVNET-2RG \
    --nsg-name myNSG2 \
    --name myNSGRuleSSH \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 22 \
    --access allow \
    --priority 200

# STEP-6 : Create network interfaces for the virtual machines

    az network nic create \
        --resource-group myVNET-1RG \
        --name nicVM1 \
        --vnet-name Vnet-1 \
        --subnet default \
        --network-security-group myNSG1

    az network nic create \
        --resource-group myVNET-2RG \
        --name nicVM2 \
        --vnet-name Vnet-2 \
        --subnet default \
        --network-security-group myNSG2

# STEP-7 : Provision VM-1 and VM-2 

az vm create \
    --resource-group myVNET-1RG \
    --name myVM1 \
    --nics nicVM1 \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 

az vm create \
    --resource-group myVNET-2RG \
    --name myVM2 \
    --nics nicVM2 \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 
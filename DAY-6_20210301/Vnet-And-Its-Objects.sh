#!/bin/bash 

# Create Variables for ResourceGroup and Region
RgName="MyResourceGroup"
Location="eastus"

# Create a Resource Group 
az group create \
--name $RgName \
--location $Location

# Create a Virtual Network with a Front-End Subnet
az network vnet create \
--name devops-Vnet \
--resource-group $RgName \
--location $Location \
--address-prefix 10.0.0.0/16 \
--subnet-name MySubnet-FrontEnd \
--subnet-prefix 10.0.1.0/24 

# Create a Back-End Subnet 
az network vnet subnet create \
--address-prefixes 10.0.2.0/24 \ 
--name MySubnet-BackEnd \
--resource-group $RgName \
--vnet-name devops-Vnet

az network vnet subnet create \
  --address-prefix 10.0.2.0/24 \
  --name MySubnet-BackEnd \
  --resource-group $RgName \
  --vnet-name devops-vnet

az network vnet subnet create --address-prefixes 10.0.2.0/24  --name MySubnet-BackEnd --resource-group $RgName --location $Location --vnet-name devops-Vnet

# Create a Network Security Group for the Front-End Subnet
az network nsg create \
--resource-group $RgName \
--name MyNsg-FrontEnd \
--location $Location

# Create a NSG rule to allow HTTP Traffic in From the Internet to the Front-End Subnet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-FrontEnd \
--name Allow-HTTP-All \
--access Allow \
--protocol Tcp \
--direction Inbound \
--priority 100 \
--source-address-prefix Internet \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 80 

# Create a NSG rule to allow SSH Traffic in From the Internet to the Front-End Subnet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-FrontEnd \
--name Allow-SSH-All \
--access Allow \
--protocol Tcp \
--direction Inbound \
--priority 300 \
--source-address-prefix Internet \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 22

# Associate the Front-End NSG to The Front-End Subnet 
az network vnet subnet update \
--vnet-name devops-Vnet \
--name MySubnet-FrontEnd \
--resource-group $RgName \
--network-security-group MyNsg-FrontEnd

# Create a Network Security Group for the Back-End Subnet
az network nsg create \
--resource-group $RgName \
--name MyNsg-BackEnd \
--location $Location

# Create a NSG rule to allow SSH Traffic in From the Internet to the Front-End Subnet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-BackEnd \
--name Allow-SSH-All \
--access Allow \
--protocol Tcp \
--direction Inbound \
--priority 200 \
--source-address-prefix Internet \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 22

# Create a NSG rule to allow MYSQL Traffic in From the Front-End Subnet to the Back-End Subnet
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-BackEnd \
--name Allow-MySql-All \
--access Allow  \
--protocol Tcp \
--direction Inbound \
--priority 100 \
--source-address-prefix 10.0.1.0/24 \
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range 3306

# Create an NSG Rule to block all OutBound Traffic from the Back-End Subnet to Internet 
az network nsg rule create \
--resource-group $RgName \
--nsg-name MyNsg-BackEnd \
--name Deny-Internet-All \
--access Deny --protocol Tcp \
--direction Outbound --priority 300 \ 
--source-address-prefix "*" \ 
--source-port-range "*" \
--destination-address-prefix "*" \
--destination-port-range "*"

# Associate the Back-End NSG to The Back-End Subnet 
az network vnet subnet update \
--vnet-name devops-Vnet \
--name MySubnet-BackEnd \
--resource-group $RgName \
--network-security-group MyNsg-BackEnd

# Create a Public IP address for the Webserver Virtual Machine 
az network public-ip create \
--resource-group $RgName \
--name MyPublicIP-Web 

# Create a NIC for the Webserver Virtual Machine 
az network nic create \
--resource-group $RgName \
--name MyNic-Web \
--vnet-name devops-Vnet \
--subnet MySubnet-FrontEnd \
--network-security-group MyNsg-FrontEnd \
--public-ip-address MyPublicIP-Web 

# Create a Web Server Virtual Machine in the Front-End Subnet
az vm create \
--resource-group $RgName \
--name MyVm-Web \
--nics MyNic-Web \
--image UbuntuLTS \
--admin-username azureadmin \
--admin-password Azure@123456 \
--custom-data cloud-web-init.txt \
--size Standard_D2s_v3 \
--data-disk-sizes-gb 128 128 \
--storage-sku Standard_LRS

# Create a Public IP address for the MySQL Database Virtual Machine  
az network public-ip create \
--resource-group $RgName \
--name MyPublicIP-Sql 

# Create a NIC for the MySQL Database Virtual Machine 
az network nic create \
--resource-group $RgName \
--name MyNic-Sql \
--vnet-name devops-Vnet \
--subnet MySubnet-BackEnd \
--network-security-group MyNsg-BackEnd \
--public-ip-address MyPublicIP-Sql 

# Create a MySQL Database Server Virtual Machine in the Back-End Subnet
az vm create \
--resource-group $RgName \
--name MyVm-Sql \
--nics MyNic-Sql \
--image UbuntuLTS \
--admin-username azureadmin \
--admin-password Azure@123456 \
--custom-data cloud-db-init.txt \
--size Standard_D2s_v3 \
--data-disk-sizes-gb 128 128 \
--storage-sku Standard_LRS
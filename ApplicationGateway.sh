# STEP-1 : Create resource group 

az group create --name myResourceGroupAG --location eastus

# STEP-2 : Create network resources

az network vnet create \
  --name myVNet \
  --resource-group myResourceGroupAG \
  --location eastus \
  --address-prefix 10.0.0.0/16 \
  --subnet-name myAGSubnet \
  --subnet-prefix 10.0.1.0/24

# STEP-3 : Create Backend Subnet 
az network vnet subnet create \
  --name myBackendSubnet \
  --resource-group myResourceGroupAG \
  --vnet-name myVNet   \
  --address-prefix 10.0.2.0/24

# STEP-4 : Create Public Ipaddress for Application Gateway

az network public-ip create \
  --resource-group myResourceGroupAG \
  --name myAGPublicIPAddress \
  --allocation-method Dynamic \
  --sku Basic 

# STEP-5 : Create network interfaces for the virtual machines

for i in `seq 1 2`; do
  az network nic create \
    --resource-group myResourceGroupAG \
    --name myNic$i \
    --vnet-name myVNet \
    --subnet myBackendSubnet
done 

# STEP-6 : Create virtual machines

# File Name : cloud-init.txt

#cloud-config
package_upgrade: true
packages:
  - nginx
  - nodejs
  - npm
write_files:
  - owner: www-data:www-data
  - path: /etc/nginx/sites-available/default
    content: |
      server {
        listen 80;
        location / {
          proxy_pass http://localhost:3000;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection keep-alive;
          proxy_set_header Host $host;
          proxy_cache_bypass $http_upgrade;
        }
      }
  - owner: azureuser:azureuser
  - path: /home/azureuser/myapp/index.js
    content: |
      var express = require('express')
      var app = express()
      var os = require('os');
      app.get('/', function (req, res) {
        res.send('Hello World from host ' + os.hostname() + '!')
      })
      app.listen(3000, function () {
        console.log('Hello world app listening on port 3000!')
      })
runcmd:
  - service nginx restart
  - cd "/home/azureuser/myapp"
  - npm init
  - npm install express -y
  - nodejs index.js

# VM Creation :

  az vm create \
    --resource-group myResourceGroupAG \
    --name myVM1 \
    --nics myNic1 \
    --image UbuntuLTS \
    --admin-username azureuser \
    --admin-password Azure@123456 \
    --custom-data cloud-init.txt \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 

  az vm create \
    --resource-group myResourceGroupAG \
    --name myVM2 \
    --nics myNic2 \
    --image UbuntuLTS \
    --admin-username azureuser \
    --admin-password Azure@123456 \
    --custom-data cloud-init.txt \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 

# STEP-7 : Create the application gateway

address1=$(az network nic show --name myNic1 --resource-group myResourceGroupAG | grep "\"privateIpAddress\":" | grep -oE '[^ ]+$' | tr -d '",')
address2=$(az network nic show --name myNic2 --resource-group myResourceGroupAG | grep "\"privateIpAddress\":" | grep -oE '[^ ]+$' | tr -d '",')

az network application-gateway create \
  --name myAppGateway \
  --location eastus \
  --resource-group myResourceGroupAG \
  --capacity 2 \
  --sku Standard_Small \
  --http-settings-cookie-based-affinity Enabled \
  --public-ip-address myAGPublicIPAddress \
  --vnet-name myVNet \
  --subnet myAGSubnet \
  --servers "$address1" "$address2"

# STEP-8 : Test the application gateway

az network public-ip show \
  --resource-group myResourceGroupAG \
  --name myAGPublicIPAddress \
  --query [ipAddress] \
  --output tsv

# STEP-9 : Go to Browser and Test 

# STEP-10 : Clean up resources

# az group delete --name myResourceGroupAG

#!/bin/bash

# Create a resource group
az group create --name myStorageRG --location eastus

# Create a storage account
az storage account create \
    --name cloudbinary1 \
    --resource-group myStorageRG \
    --location eastus \
    --sku Standard_LRS \
    --encryption-services blob

# Note : Blobs, Files, Tables & Queues 

# Create a container
Folder/
    Data.csv 
    Folder1/
        report-infra.txt
        Folder2/
            security-report.xlx
            Folder3
                architecture-flow.jpeg 

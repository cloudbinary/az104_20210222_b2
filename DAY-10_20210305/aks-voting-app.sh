#!/bin/bash

# Deploy an Azure Kubernetes Service cluster using the Azure CLI

# Create a resource group
az group create --name myResourceGroup --location eastus

# Create AKS cluster
az provider show -n Microsoft.OperationsManagement -o table
az provider show -n Microsoft.OperationalInsights -o table

az provider register --namespace Microsoft.OperationsManagement
az provider register --namespace Microsoft.OperationalInsights

az aks create --resource-group myResourceGroup --name myAKSCluster --node-count 3 --enable-addons monitoring --generate-ssh-keys

# Connect to the cluster
az aks install-cli

az aks get-credentials --resource-group myResourceGroup --name myAKSCluster


# aks commands 
kubectl get nodes


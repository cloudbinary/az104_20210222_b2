#!/bin/bash 

# STEP-1 : RG 

az group create \
    --name demok8sRG \
    --location eastus

# STEP-2 : Vnet - Create a virtual network

az network vnet create \
    --resource-group demok8sRG \
    --location eastus \
    --name myVNet \
    --address-prefixes 10.1.0.0/16 \
    --subnet-name myBackendSubnet \
    --subnet-prefixes 10.1.0.0/24

# STEP-3 : Create a network security group

az network nsg create \
    --resource-group demok8sRG \
    --name myNSG

# STEP-4 : Create a network security group rule

az network nsg rule create \
    --resource-group demok8sRG \
    --nsg-name myNSG \
    --name myNSGRuleHTTP \
    --protocol '*' \
    --direction inbound \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range '*' \
#    --destination-port-range 80 \
    --access allow \
    --priority 200

# STEP-5 : Create virtual machines

az vm create \
    --resource-group demok8sRG \
    --name k8s-master \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 \
    --custom-data cloud-k8s-init.txt \
    --no-wait

az vm create \
    --resource-group demok8sRG \
    --name k8s-node1 \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 \
    --custom-data cloud-k8s-init.txt \
    --no-wait

az vm create \
    --resource-group demok8sRG \
    --name k8s-node1 \
    --image UbuntuLTS \
    --admin-username azureadmin \
    --admin-password Azure@123456 \
    --storage-sku Standard_LRS \
    --size Standard_D2s_v3 \
    --data-disk-sizes-gb 128 128 \
    --custom-data cloud-k8s-init.txt \
    --no-wait


# Clean up resources
# az group delete --name demok8sRG

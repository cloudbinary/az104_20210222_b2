<#

az network vnet create

Create a virtual network.

You may also create a subnet at the same time by specifying a subnet name and (optionally) an address prefix.

az network vnet create --name
                       --resource-group
                       [--address-prefixes]
                       [--ddos-protection {false, true}]
                       [--ddos-protection-plan]
                       [--defer]
                       [--dns-servers]
                       [--location]
                       [--network-security-group]
                       [--subnet-name]
                       [--subnet-prefixes]
                       [--subscription]
                       [--tags]
                       [--vm-protection {false, true}]

% az network vnet create -g devopsRG -n avaya-tst_vnet

% az network vnet create -g devopsRG -n avaya-tst_vnet --address-prefixes 192.168.0.0/16 --subnet-name webLayer_subnet --subnet-prefix 192.168.1.0/24 

#>
<#


New-AzVirtualNetwork -Name prod_vnet 
-ResourceGroupName devopsRG 
-Location westus 
-AddressPrefix 172.17.0.0/16 
-Subnet 

Creating Variables & Calling Variables in a Command :

$Location="westus"
$Subnet=New-AzVirtualNetworkingSubnetConfig -Name default -AddressPrefix 172.16.1.0/24
$Resourcegroup="devopsRG"

$Subnet=New-AzVirtualNetworkingSubnetConfig -Name default -AddressPrefix 172.18.1.0/24 New-AzVirtualNetwork -Name avaya-prod_vnet -ResourceGroupName $Resourcegroup -Location $Location -AddressPrefix 172.18.0.0/16 -Subnet $Subnet 

#>

PS /home/cs-user-272b4825a12a> cat vnet-example.ps1
$Location="WestUS"
New-AzResourceGroup -Name vm-networks -Location $Location

$Subnet=New-AzVirtualNetworkSubnetConfig -Name default -AddressPrefix 10.5.0.0/24
 New-AzVirtualNetwork -Name myVnet -ResourceGroupName vm-networks -Location $Location -AddressPrefix 10.5.0.0/16 -Subnet $Subnet
 
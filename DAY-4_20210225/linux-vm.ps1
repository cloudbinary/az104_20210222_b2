<#
Create a Linux virtual machine in Azure

You can alter configuration, manage networks, open or block traffic, and more through the 

1. Azure portal, 
2. the Azure CLI, or 
3. Azure PowerShell tools.


Linux / Unix Servers : 22 SSH 

1. Create a network security group.
2. Create an inbound rule allowing traffic on the ports you need.

What is a network security group? 

Virtual networks (VNets) are the foundation of the Azure networking model and provide isolation and protection. Network security groups (NSGs) are the primary tool you use to enforce and control network traffic rules at the networking level. NSGs are an optional security layer that provides a software firewall by filtering inbound and outbound traffic on the VNet.

Security group rules:
NSGs use rules to allow or deny traffic moving through the network. Each rule identifies the source and destination address (or range), protocol, port (or range), direction (inbound or outbound), a numeric priority, and whether to allow or deny the traffic that matches the rule.


Security group rules :
NSGs use rules to allow or deny traffic moving through the network. 
Each rule identifies the source and destination address (or range), protocol, port (or range), direction (inbound or outbound), a numeric priority, 
and whether to allow or deny the traffic that matches the rule.


Launch a Linux VM and Access Website :

STEP-1 : Launch Azure Cloud Shell

STEP-2 : Create a Resource Group / Use Exising 

STEP-3 : Create a Virtual Machine 

STEP-4 : Open Port 22 & Port 80

STEP-5 : Connect Virtual Machine from your Local Machine 

STEP-6 : Install WebServer - Apache apache2 [ apt update && apt install apache2 -y ]

STEP-7 : Open a Browser and Access a Default Site 

STEP-8 : Validate 

STEP-9 : Clean-up


#!/bin/bash
sudo apt update 
sudo apt install apache2 -y 
echo "Welcome to Azure World" > /var/www/html/index.html 

#>

Create a resource group : 

az group create --name myResourceGroup --location eastus

Create virtual machine : 

az vm create --resource-group webserverRG --name FirstWebSite --image UbuntuLTS --size Standard_B1ms --admin-username azureadmin --admin-password Azure@123456 --custom-data cloud-init.txt

az vm create --resource-group webserverRG --name WinWebServer --image win2016datacenter --admin-username azureadmin --admin-password Azure@123456 

Win2016Datacenter

Open port 80 for web traffic : 
az vm open-port --port 80 --resource-group webserverRG --name WinWebServer

Install-WindowsFeature -name Web-Server -IncludeManagementTools

View the web server in action : 
open Browser ---> ipaddress 


#----------------------------------------------------------------#
Create a resource group : 

az group create --name vm-RG --location westus 

Create virtual machine : 

cloud-linux-init.txt

#!/bin/bash
sudo apt update 
sudo apt install apache2 -y 
echo "Welcome to Azure World" > /var/www/html/index.html 

az vm create --resource-group vm-RG --name FirstWebSite --image UbuntuLTS --admin-username azureadmin --admin-password Azure@123456 --custom-data cloud-linux-init.txt

cloud-win-init.txt
Install-WindowsFeature -name Web-Server -IncludeManagementTools

az vm create --resource-group vm-RG --name WinWebServer --image win2016datacenter --admin-username azureadmin --admin-password Azure@123456 --custom-data cloud-win-init.txt
az vm create --resource-group vm-RG --name FirstWebSite --image UbuntuLTS --size Standard_B1ms --admin-username azureadmin --admin-password Azure@123456 --custom-data cloud-init.txt

Open port 80 for web traffic : 
az vm open-port --port 80 --resource-group webserverRG --name WinWebServer

Install-WindowsFeature -name Web-Server -IncludeManagementTools

View the web server in action : 
open Browser ---> ipaddress 
#----------------------------------------------------------------#